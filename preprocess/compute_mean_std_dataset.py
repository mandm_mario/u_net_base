import cv2
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard
from sklearn.model_selection import train_test_split

import sys
from keras.applications.vgg16 import preprocess_input
import params

input_size = params.input_size
base_dir = params.BASE_DIR

df_train = pd.read_csv('../input/train_masks.csv')

ids_train = df_train['img'].map(lambda s: s.split('.')[0])

images_r = []
images_g = []
images_b = []
for id in range(0, len(ids_train), 1):
    img = cv2.imread('E:\\Repo\\kaggle_segmentation\\kaggle_segmentation\\input\\train\\{}.jpg'.format(ids_train[id]))
    b, g, r = cv2.split(img)
    images_b.append(b)
    images_g.append(g)
    images_r.append(r)

mean_r = np.mean(images_r,axis=0)
mean_g = np.mean(images_g,axis=0)
mean_b = np.mean(images_b,axis=0)

std_r = np.mean(images_r,axis=0)
std_g = np.mean(images_g,axis=0)
std_b = np.mean(images_b,axis=0)

np.save("mean_r.npy",mean_r)
np.save("mean_g.npy",mean_g)
np.save("mean_b.npy",mean_b)

print(mean_r.shape)

np.save("std_r.npy",std_r)
np.save("std_g.npy",std_g)
np.save("std_b.npy",std_b)

print(std_r.shape)