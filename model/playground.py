import csv
maskcsv = open('test.csv', 'w', newline='')
writer = csv.writer(maskcsv,skipinitialspace=True, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
writer.writerow(['img', 'rle_mask'])
writer.writerow(['1'])
writer.writerow(['2'])
maskcsv.close()