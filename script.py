
import os
import csv
import shutil
from PIL import Image, ImageEnhance
import plotly.plotly as py
import plotly.graph_objs as go



def test_csv_generator(ids_valid):
    print('Generating validation.csv')
    maskcsv = open('test/validation.csv', 'w', newline='')
    writer = csv.writer(maskcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['img', 'rle_mask'])
    for valid in ids_valid:
        writer.writerow([valid + '.png'])
    print('Generated')



def scripty():
    maskcsv = open('train_masks/prueba.csv', 'w', newline='')
    writer = csv.writer(maskcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['img', 'rle_mask'])
    with open('correspondences.csv', 'r') as correspondecescsv:
        data = list(list(rec) for rec in csv.reader(correspondecescsv))
        length = len(data)
        i = 1
        origin = 'bd/'
        dest_input = 'train/'
        dest_mask = 'train_masks/'
        while i<length :
            if (i + 1 < length) and ((data[i + 1])[0] == (data[i])[0]):
                img1 = Image.open('bd/' + (data[i])[2].split('.')[0] + '_segmentation_full.png')
                i += 1
                """
                img2 = Image.open('bd/' + (data[i])[2].split('.')[0] + '_segmentation_full.png')
                full = Image.open('bd/' + (data[i])[2].split('.')[0] + '_full.png')
                """
                name = (data[i-1])[2].split('.')[0] + '+' + (data[i])[2].split('.')[0]

                #finalmask = ImageEnhance.Brightness(Image.blend(img1, img2, alpha=0.5)).enhance(2)

                while (i + 1 < length) and ((data[i + 1])[0] == (data[i])[0]):
                    i += 1
                    """img3 = Image.open('bd/' + (data[i])[2].split('.')[0] + '_segmentation_full.png')
                    finalmask = ImageEnhance.Brightness(Image.blend(finalmask, img3, alpha=0.5)).enhance(2)
                    """
                    name = name + '+' + (data[i])[2].split('.')[0]

                #print(name)
                """
                full.save('train/' + name + '.png')
                finalmask.save('train_masks/'+name+'_mask.png')
                """
                writer.writerow([name+'.png'])
                print(name)
                i+=1

            else:
                """
                shutil.copy('bd/' + (data[i])[2].split('.')[0] + '_full.png', dest_input+(data[i])[2].split('.')[0] + '.png')
                shutil.copy('bd/' + (data[i])[2].split('.')[0] + '_segmentation_full.png', dest_mask+(data[i])[2].split('.')[0] +'_mask.png')
                """
                print((data[i])[2].split('.')[0])
                writer.writerow([(data[i])[2].split('.')[0]+'.png'])
                i+= 1

        maskcsv.close()


