from model.u_net import get_unet_128, get_unet_256, get_unet_512, get_unet_1024

input_size = 1024

max_epochs = 500
batch_size = 1

#orig_width = 1918
#orig_height = 1280

threshold = 0.8

# ignorar parametros
use_bbox = False
use_cropping = False
isolate_views = False

#BASE_DIR = 'D:\\Box Sync\\kaggle_segmentation\\'
#BASE_DIR = "G:\\Git\\u_net_base"
#BASE_DIR = "E:\\Repo\\kaggle_segmentation\\kaggle_segmentation\\input"
BASE_DIR =  '/mnt/md0/mmartinez/unet'
model = get_unet_1024()
title =  'unet_1024'
